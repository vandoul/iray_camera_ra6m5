/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author        Notes
 * 2021-10-10     Sherman       first version
 */

#include <rtthread.h>
#include "hal_data.h"
#include <rtdevice.h>
#ifdef PKG_USING_CMBACKTRACE
#include "cm_backtrace.h"
#endif

#define LED_PIN    BSP_IO_PORT_04_PIN_00 /* RED LED pins */
#define SW3_PIN    BSP_IO_PORT_00_PIN_05 /* SW3 pins */
#define LV_INDEV_TOUCHPAD_IRQ_PIN           BSP_IO_PORT_09_PIN_06
static void btn_callback(void *user_data)
{
    rt_kprintf("btn2\r\n");
}
static void tp_callback(void *user_data)
{
    rt_kprintf("tp int\r\n");
}
void hal_entry(void)
{
    #ifdef PKG_USING_CMBACKTRACE
    cm_backtrace_init("iray_cam", "V1.0.0", "V1.0.0");
    #endif
    rt_kprintf("\nHello RT-Thread!\n");
    rt_pin_mode(SW3_PIN, PIN_MODE_INPUT_PULLUP);
    rt_pin_attach_irq(SW3_PIN, PIN_IRQ_MODE_FALLING, btn_callback, RT_NULL);
    rt_pin_irq_enable(SW3_PIN, PIN_IRQ_ENABLE);
//    rt_pin_mode(LV_INDEV_TOUCHPAD_IRQ_PIN, PIN_MODE_INPUT_PULLUP);
//    rt_pin_attach_irq(LV_INDEV_TOUCHPAD_IRQ_PIN, PIN_IRQ_MODE_FALLING, tp_callback, RT_NULL);
//    rt_pin_irq_enable(LV_INDEV_TOUCHPAD_IRQ_PIN, PIN_IRQ_ENABLE);

    while (1)
    {
        rt_pin_write(LED_PIN, PIN_HIGH);
        rt_thread_mdelay(500);
        rt_pin_write(LED_PIN, PIN_LOW);
        rt_thread_mdelay(500);
    }
}