/*
 * Copyright (c) 2006-2022, RT-Thread Development Team
 *
 * SPDX-License-Identifier: MIT
 *
 * Change Logs:
 * Date           Author        Notes
 * 2022-05-13     Meco Man      First version
 */

#ifdef __RTTHREAD__
#include "custom.h"
#include "events_init.h"
lv_ui guider_ui = {0};
void lv_user_gui_init(void)
{
//    extern void ui_init(void);
//    ui_init();

//    extern void custom_init(lv_ui *ui);
//    custom_init(&guider_ui);
    setup_ui(&guider_ui);
    events_init(&guider_ui);
    custom_init(&guider_ui);
}

#endif /* __RTTHREAD__ */
