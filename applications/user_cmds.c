
#include <rtthread.h>
#include <finsh.h>
#include <stdlib.h>

#include "ft6236.h"
#include "lvgl.h"

static rt_sem_t rx_sem = RT_NULL;
rt_err_t tp_rx_ind(rt_device_t dev, rt_size_t size)
{
//    rt_kprintf("tp rx: %d\r\n", size);
    rt_sem_release(rx_sem);
    return RT_EOK;
}
static void tp_test(void *p)
{
    struct rt_touch_data point;
    rt_device_t tp = (rt_device_t)p;
    RT_ASSERT(tp != NULL);
    RT_ASSERT(rx_sem != NULL);
    while(1)
    {
        rt_sem_take(rx_sem, RT_WAITING_FOREVER);
        if(rt_device_read(tp, 0, &point, 1) == 1)
        {
            int x = point.x_coordinate;
            int y = point.y_coordinate;
            int event = point.event;
            switch(event)
            {
                case RT_TOUCH_EVENT_NONE:
//                    rt_kprintf("%d:(%d, %d)\r\n", point.timestamp, x, y);
                    event = LITTLEVGL2RTT_INPUT_NONE;
                    break;
                case RT_TOUCH_EVENT_UP  :
//                    rt_kprintf("%d:(%d, %d)up\r\n", point.timestamp, x, y);
                    event = LITTLEVGL2RTT_INPUT_UP;
                    break;
                case RT_TOUCH_EVENT_DOWN:
//                    rt_kprintf("%d:(%d, %d)down\r\n", point.timestamp, x, y);
                    event = LITTLEVGL2RTT_INPUT_DOWN;
                    break;
                case RT_TOUCH_EVENT_MOVE:
//                    rt_kprintf("%d:(%d, %d)mov\r\n", point.timestamp, x, y);
                    event = LITTLEVGL2RTT_INPUT_MOVE;
                    break;
            }
            littlevgl2rtt_send_input_event(x, y, event);
        }
    }
}

static void ft6236_help(void)
{
    rt_kprintf("ft6236 init <name> <bus_name> [rst_pin]\r\n");
}
void ft6236_init(char *bus, int pin)
{
    const char *name = "tp";
    struct rt_touch_config cfg = 
    {
        .irq_pin = {.pin = 0x906, .mode = PIN_MODE_INPUT_PULLUP},
        .dev_name   = bus,
        .user_data  = RT_NULL,
    };
    if(RT_EOK == rt_hw_ft6236_init(name, &cfg,  pin))
    {
        rt_kprintf("init ok\r\n");
        rt_device_t tp = rt_device_find(name);
        if(tp)
        {
            rt_device_set_rx_indicate(tp, tp_rx_ind);
            if(RT_EOK != rt_device_open(tp, RT_DEVICE_FLAG_RDONLY|RT_DEVICE_FLAG_INT_RX))
            {
                rt_kprintf("open tp failed!\r\n");
            }
            else
            {
                rx_sem = rt_sem_create("tpsem", 0, RT_IPC_FLAG_FIFO);
                rt_thread_t tid = rt_thread_create("tptest", tp_test, tp, 1024, 5, 10);
                if(tid)
                {
                    rt_thread_startup(tid);
                }
            }
        }
        else
        {
            rt_kprintf("tp %s not found\r\n", name);
        }
    }
    else
    {
        rt_kprintf("init failed\r\n");
    }
}
int ft6236(int argc, char *argv[])
{
    if(argc > 2)
    {
        if(rt_strcmp(argv[1], "init") == 0)
        {
            if(argc > 3)
            {
                const char *name = argv[2];
                char *bus = argv[3];
                int pin = 781;
                if(argc > 4)
                {
                    pin = atoi(argv[4]);
                }
                ft6236_init(bus, pin);
            }
            else
            {
                ft6236_help();
            }
        }
    }
    else
    {
        ft6236_help();
    }
    return 0;
}
MSH_CMD_EXPORT(ft6236, ft6236 test cmd.);

static int iray_cam_cmd(int argc, char *argv[])
{
    if(argc > 2)
    {
        if(rt_strcmp(argv[1], "run") == 0)
        {
            void iray_camera_init(const char *bus);
            void ft6236_init(char *bus, int pin);
            int pin = 90;
            char *bus = argv[2];
            if(argc > 3)
            {
                pin = atoi(argv[3]);
            }
            ft6236_init(bus, pin);
            while(1)
            {
                rt_device_t tp = rt_device_find("tp");
                if(tp != NULL)
                {
                    break;
                }
                rt_thread_mdelay(100);
            }
            rt_thread_mdelay(200);
            iray_camera_init(bus);
            return RT_EOK; 
        }
    }
    rt_kprintf("usage: iray_cam run <i2c bus> <tp int>\r\n");
    return RT_EOK; 
}
//INIT_APP_EXPORT(rt_lvgl_demo_init); 
MSH_CMD_EXPORT_ALIAS(iray_cam_cmd, iray_cam,iray camera);

