
// file system
#include "fcntl.h"
#include "sys/unistd.h"
int libbmp_file_write(void *file, int offset, void *buf, int size)
{
    return write((int)file, buf, size);
}
int libbmp_file_read(void *file, int offset, void *buf, int size)
{
    return read((int)file, buf, size);
}
int libbmp_file_seek(void *file, int offset, int pos)
{
    return lseek((int)file, offset, pos);
}


