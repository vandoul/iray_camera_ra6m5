#include <rtthread.h>

static inline int str2hex_d(char c)
{
    if(c < '0' || c > '9')
    {
        return -1;
    }
    return (c - '0');
}
static inline int str2hex_x(char c)
{
    if(c < 'a' || c > 'f')
    {
        return str2hex_d(c);
    }
    return (c - 'W'); //'W' = 'a' - 10
}
rt_uint32_t str2hex(const char *str)
{
    volatile rt_uint32_t value = 0;
    volatile rt_uint8_t scale = 0;
    volatile int i;
    int (*volatile func)(char) = RT_NULL;
    if(str[0] == '0' && (str[1] == 'x' || str[1] == 'X'))
    {
        i = 2;
        func = str2hex_x;
        scale = 16;
    }
    else
    {
        i = 0;
        func = str2hex_d;
        scale = 10;
    }
    for(;str[i];i++)
    {
        int v = func(str[i]|0x20);
        if(v == -1)
        {
            break;
        }
        value *= scale;
        value += v;
    }
    return value;
}