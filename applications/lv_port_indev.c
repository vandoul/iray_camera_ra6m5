/**
 * @file lv_port_indev_templ.c
 *
 */

/*Copy this file as "lv_port_indev.c" and set this value to "1" to enable content*/
#if 1

/*********************
 *      INCLUDES
 *********************/
#include "lv_port_indev.h"
#include "../../lvgl.h"

/**********************
 *      MACROS
 **********************/
#define LV_INDEV_USING_TOUCHPAD
//#define LV_INDEV_USING_MOUSE
//#define LV_INDEV_USING_KEYPAD
//#define LV_INDEV_USING_ENCODER
//#define LV_INDEV_USING_BUTTON

/*********************
 *      DEFINES
 *********************/

/**********************
 *      TYPEDEFS
 **********************/

/**********************
 *  STATIC PROTOTYPES
 **********************/

static void touchpad_init(void);
static void touchpad_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data);

static void mouse_init(void);
static void mouse_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data);
static bool mouse_is_pressed(void);
static void mouse_get_xy(lv_coord_t * x, lv_coord_t * y);

static void keypad_init(void);
static void keypad_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data);
static uint32_t keypad_get_key(void);

static void encoder_init(void);
static void encoder_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data);
static void encoder_handler(void);

static void button_init(void);
static void button_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data);
static int8_t button_get_pressed_id(void);
static bool button_is_pressed(uint8_t id);

/**********************
 *  STATIC VARIABLES
 **********************/
#ifdef LV_INDEV_USING_TOUCHPAD
lv_indev_t * indev_touchpad;
#endif
#ifdef LV_INDEV_USING_MOUSE
lv_indev_t * indev_mouse;
#endif
#ifdef LV_INDEV_USING_KEYPAD
lv_indev_t * indev_keypad;
#endif
#ifdef LV_INDEV_USING_ENCODER
lv_indev_t * indev_encoder;
#endif
#ifdef LV_INDEV_USING_BUTTON
lv_indev_t * indev_button;
#endif

/**********************
 *   GLOBAL FUNCTIONS
 **********************/

void lv_port_indev_init(void)
{
    /**
     * Here you will find example implementation of input devices supported by LittelvGL:
     *  - Touchpad
     *  - Mouse (with cursor support)
     *  - Keypad (supports GUI usage only with key)
     *  - Encoder (supports GUI usage only with: left, right, push)
     *  - Button (external buttons to press points on the screen)
     *
     *  The `..._read()` function are only examples.
     *  You should shape them according to your hardware
     */

    static lv_indev_drv_t indev_drv;

    /*------------------
     * Touchpad
     * -----------------*/
#ifdef LV_INDEV_USING_TOUCHPAD
    /*Initialize your touchpad if you have*/
    touchpad_init();

    /*Register a touchpad input device*/
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read_cb = touchpad_read;
    indev_touchpad = lv_indev_drv_register(&indev_drv);
#endif
    /*------------------
     * Mouse
     * -----------------*/
#ifdef LV_INDEV_USING_MOUSE
    /*Initialize your mouse if you have*/
    mouse_init();

    /*Register a mouse input device*/
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_POINTER;
    indev_drv.read_cb = mouse_read;
    indev_mouse = lv_indev_drv_register(&indev_drv);

    /*Set cursor. For simplicity set a HOME symbol now.*/
    lv_obj_t * mouse_cursor = lv_img_create(lv_scr_act());
    lv_img_set_src(mouse_cursor, LV_SYMBOL_HOME);
    lv_indev_set_cursor(indev_mouse, mouse_cursor);
#endif
    /*------------------
     * Keypad
     * -----------------*/
#ifdef LV_INDEV_USING_KEYPAD
    /*Initialize your keypad or keyboard if you have*/
    keypad_init();

    /*Register a keypad input device*/
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_KEYPAD;
    indev_drv.read_cb = keypad_read;
    indev_keypad = lv_indev_drv_register(&indev_drv);

    /*Later you should create group(s) with `lv_group_t * group = lv_group_create()`,
     *add objects to the group with `lv_group_add_obj(group, obj)`
     *and assign this input device to group to navigate in it:
     *`lv_indev_set_group(indev_keypad, group);`*/
#endif
    /*------------------
     * Encoder
     * -----------------*/
#ifdef LV_INDEV_USING_ENCODER
    /*Initialize your encoder if you have*/
    encoder_init();

    /*Register a encoder input device*/
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_ENCODER;
    indev_drv.read_cb = encoder_read;
    indev_encoder = lv_indev_drv_register(&indev_drv);

    /*Later you should create group(s) with `lv_group_t * group = lv_group_create()`,
     *add objects to the group with `lv_group_add_obj(group, obj)`
     *and assign this input device to group to navigate in it:
     *`lv_indev_set_group(indev_encoder, group);`*/
#endif
    /*------------------
     * Button
     * -----------------*/
#ifdef LV_INDEV_USING_BUTTON
    /*Initialize your button if you have*/
    button_init();

    /*Register a button input device*/
    lv_indev_drv_init(&indev_drv);
    indev_drv.type = LV_INDEV_TYPE_BUTTON;
    indev_drv.read_cb = button_read;
    indev_button = lv_indev_drv_register(&indev_drv);

    /*Assign buttons to points on the screen*/
    static const lv_point_t btn_points[2] = {
        {10, 10},   /*Button 0 -> x:10; y:10*/
        {40, 100},  /*Button 1 -> x:40; y:100*/
    };
    lv_indev_set_button_points(indev_button, btn_points);
#endif
}

/**********************
 *   STATIC FUNCTIONS
 **********************/

/*------------------
 * Touchpad
 * -----------------*/
#ifdef LV_INDEV_USING_TOUCHPAD
#include "ipc/workqueue.h"
#include "hal_data.h"
#include <rtdevice.h>
#include "ft6236.h"
#include "lcd_st7789.h"
extern struct rt_device_graphic_info disp_info;
#define LV_INDEV_TOUCHPAD_BUS_NAME          "sci2c6"
#define LV_INDEV_TOUCHPAD_RST_PIN           BSP_IO_PORT_03_PIN_13
#define LV_INDEV_TOUCHPAD_IRQ_PIN           BSP_IO_PORT_09_PIN_06
static rt_bool_t touchpad_is_pressed = RT_FALSE;
static struct rt_work tp_work;
struct rt_workqueue *lv_indev_wq;
static lv_coord_t last_x = 0;
static lv_coord_t last_y = 0;
/* touch event callback */
static rt_err_t tp_event_callback(rt_device_t dev, rt_size_t size)
{
    if(lv_indev_wq)
    {
        rt_workqueue_dowork(lv_indev_wq, &tp_work);
    }
    return RT_EOK;
}
static void tp_work_callback(struct rt_work *wk, void *user_data)
{
    struct rt_touch_data point;
    if(rt_device_read(user_data, 0, &point, 1) == 1)
    {
        #if USE_DIRECTION == 0
        last_x = point.x_coordinate;
        last_y = point.y_coordinate;
        #elif USE_DIRECTION == 1
        last_x = point.y_coordinate;
        last_y = disp_info.height - point.x_coordinate;
        #elif USE_DIRECTION == 2
        last_x = point.x_coordinate;
        last_y = point.y_coordinate;
        #elif USE_DIRECTION == 3
        last_x = point.x_coordinate;
        last_y = point.y_coordinate;
        #else
        #error "invalid USE_DIRECTION"
        #endif
        switch(point.event)
        {
            case RT_TOUCH_EVENT_UP  :
                touchpad_is_pressed = RT_FALSE;
                break;
            case RT_TOUCH_EVENT_DOWN:
                touchpad_is_pressed = RT_TRUE;
                break;
            case RT_TOUCH_EVENT_NONE:
            case RT_TOUCH_EVENT_MOVE:
                break;
        }
    }
}
/*Initialize your touchpad*/
static void touchpad_init(void)
{
    /*Your code comes here*/
    const char *name = "tp";
    struct rt_touch_config cfg = 
    {
        .irq_pin = {.pin = LV_INDEV_TOUCHPAD_IRQ_PIN, .mode = PIN_MODE_INPUT_PULLUP},
        .dev_name   = LV_INDEV_TOUCHPAD_BUS_NAME,
        .user_data  = RT_NULL,
    };
    lv_indev_wq = rt_workqueue_create("lv_indev", 1024, 10);
    if(lv_indev_wq == RT_NULL)
    {
        rt_kprintf("create lv_indev_wq failed!\r\n");
        return ;
    }
    if(RT_EOK == rt_hw_ft6236_init(name, &cfg,  LV_INDEV_TOUCHPAD_RST_PIN))
    {
        rt_kprintf("init ok\r\n");
        rt_device_t tp = rt_device_find(name);
        if(tp)
        {
            rt_device_set_rx_indicate(tp, tp_event_callback);
            if(RT_EOK != rt_device_open(tp, RT_DEVICE_FLAG_RDONLY|RT_DEVICE_FLAG_INT_RX))
            {
                rt_kprintf("open tp failed!\r\n");
            }
            else
            {
                rt_work_init(&tp_work, tp_work_callback, tp);
            }
        }
        else
        {
            rt_kprintf("tp %s not found\r\n", name);
        }
    }
    else
    {
        rt_kprintf("init failed\r\n");
    }
}

/*Will be called by the library to read the touchpad*/
static void touchpad_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    /*Save the pressed coordinates and the state*/
    if(touchpad_is_pressed)
    {
        data->state = LV_INDEV_STATE_PR;
    }
    else
    {
        data->state = LV_INDEV_STATE_REL;
    }

    /*Set the last pressed coordinates*/
    data->point.x = last_x;
    data->point.y = last_y;
}
#endif
/*------------------
 * Mouse
 * -----------------*/
#ifdef LV_INDEV_USING_MOUSE
/*Initialize your mouse*/
static void mouse_init(void)
{
    /*Your code comes here*/
}

/*Will be called by the library to read the mouse*/
static void mouse_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    /*Get the current x and y coordinates*/
    mouse_get_xy(&data->point.x, &data->point.y);

    /*Get whether the mouse button is pressed or released*/
    if(mouse_is_pressed()) {
        data->state = LV_INDEV_STATE_PR;
    }
    else {
        data->state = LV_INDEV_STATE_REL;
    }
}

/*Return true is the mouse button is pressed*/
static bool mouse_is_pressed(void)
{
    /*Your code comes here*/

    return false;
}

/*Get the x and y coordinates if the mouse is pressed*/
static void mouse_get_xy(lv_coord_t * x, lv_coord_t * y)
{
    /*Your code comes here*/

    (*x) = 0;
    (*y) = 0;
}
#endif
/*------------------
 * Keypad
 * -----------------*/
#ifdef LV_INDEV_USING_KEYPAD
/*Initialize your keypad*/
static void keypad_init(void)
{
    /*Your code comes here*/
}

/*Will be called by the library to read the mouse*/
static void keypad_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{
    static uint32_t last_key = 0;

    /*Get the current x and y coordinates*/
    mouse_get_xy(&data->point.x, &data->point.y);

    /*Get whether the a key is pressed and save the pressed key*/
    uint32_t act_key = keypad_get_key();
    if(act_key != 0) {
        data->state = LV_INDEV_STATE_PR;

        /*Translate the keys to LVGL control characters according to your key definitions*/
        switch(act_key) {
            case 1:
                act_key = LV_KEY_NEXT;
                break;
            case 2:
                act_key = LV_KEY_PREV;
                break;
            case 3:
                act_key = LV_KEY_LEFT;
                break;
            case 4:
                act_key = LV_KEY_RIGHT;
                break;
            case 5:
                act_key = LV_KEY_ENTER;
                break;
        }

        last_key = act_key;
    }
    else {
        data->state = LV_INDEV_STATE_REL;
    }

    data->key = last_key;
}

/*Get the currently being pressed key.  0 if no key is pressed*/
static uint32_t keypad_get_key(void)
{
    /*Your code comes here*/

    return 0;
}
#endif
/*------------------
 * Encoder
 * -----------------*/
#ifdef LV_INDEV_USING_ENCODER
static int32_t encoder_diff;
static lv_indev_state_t encoder_state;
/*Initialize your keypad*/
static void encoder_init(void)
{
    /*Your code comes here*/
}

/*Will be called by the library to read the encoder*/
static void encoder_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{

    data->enc_diff = encoder_diff;
    data->state = encoder_state;
}

/*Call this function in an interrupt to process encoder events (turn, press)*/
static void encoder_handler(void)
{
    /*Your code comes here*/

    encoder_diff += 0;
    encoder_state = LV_INDEV_STATE_REL;
}
#endif
/*------------------
 * Button
 * -----------------*/
#ifdef LV_INDEV_USING_BUTTON
/*Initialize your buttons*/
static void button_init(void)
{
    /*Your code comes here*/
}

/*Will be called by the library to read the button*/
static void button_read(lv_indev_drv_t * indev_drv, lv_indev_data_t * data)
{

    static uint8_t last_btn = 0;

    /*Get the pressed button's ID*/
    int8_t btn_act = button_get_pressed_id();

    if(btn_act >= 0) {
        data->state = LV_INDEV_STATE_PR;
        last_btn = btn_act;
    }
    else {
        data->state = LV_INDEV_STATE_REL;
    }

    /*Save the last pressed button's ID*/
    data->btn_id = last_btn;
}

/*Get ID  (0, 1, 2 ..) of the pressed button*/
static int8_t button_get_pressed_id(void)
{
    uint8_t i;

    /*Check to buttons see which is being pressed (assume there are 2 buttons)*/
    for(i = 0; i < 2; i++) {
        /*Return the pressed button's ID*/
        if(button_is_pressed(i)) {
            return i;
        }
    }

    /*No button pressed*/
    return -1;
}

/*Test if `id` button is pressed or not*/
static bool button_is_pressed(uint8_t id)
{

    /*Your code comes here*/

    return false;
}
#endif
#else /*Enable this file at the top*/

/*This dummy typedef exists purely to silence -Wpedantic.*/
typedef int keep_pedantic_happy;
#endif
