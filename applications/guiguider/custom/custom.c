// SPDX-License-Identifier: MIT
// Copyright 2020 NXP

/**
 * @file custom.c
 *
 */

/*********************
 *      INCLUDES
 *********************/
#include <stdio.h>
#include "lvgl.h"
#include "custom.h"

/*********************
 *      DEFINES
 *********************/
#define LINE_BOARD_WIDTH                    21
/**********************
 *      TYPEDEFS
 **********************/
typedef struct temp_cursor_object
{
    lv_obj_t *cursor;
    lv_obj_t *label;
    uint16_t x;
    uint16_t y;
}temp_cursor_object_t;

/**********************
 *  STATIC PROTOTYPES
 **********************/
//static lv_obj_t *temp_cursor_cont;
static temp_cursor_object_t temp_cursor[5] =
{
    {NULL, NULL, 100, 100},
    {NULL, NULL, 100, 100},
    {NULL, NULL, 100, 100},
    {NULL, NULL, 100, 100},
    {NULL, NULL, 100, 100},
};
static int32_t active_cursor = 1;

/**********************
 *  STATIC VARIABLES
 **********************/

/**********************
 *  STATIC FUNCTIONS
 **********************/
static void lv_custom_cursor_set_pos(temp_cursor_object_t *temp_cursor, int16_t x, int16_t y);
static void custom_cursor_event_handler(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    switch (code)
    {
    case LV_EVENT_PRESSING:
    {
        lv_obj_t *obj = lv_event_get_target(e);
        temp_cursor_object_t *cursor = lv_obj_get_user_data(obj);
        lv_indev_t *indev = lv_indev_get_act();
        if(indev == NULL) return;
        lv_obj_t* act_scr = lv_scr_act();
        lv_coord_t x_max = lv_obj_get_width(act_scr);
        lv_coord_t y_max = lv_obj_get_height(act_scr);
        #if 0
        lv_point_t vect;
        lv_indev_get_vect(indev, &vect);
        lv_coord_t x = cursor->x + vect.x;
        lv_coord_t y = cursor->y + vect.y;
        #else
        lv_point_t point;
        lv_indev_get_point(indev, &point);
        lv_coord_t x = point.x;
        lv_coord_t y = point.y;
        #endif
        if(x < 0)
        {
            x = 0;
        }
        else if(x >= x_max)
        {
            x = x_max - 1;
        }
        if (y < 0)
        {
            y = 0;
        }
        else if(y >= y_max)
        {
            y = y_max - 1;
        }
        lv_custom_cursor_set_pos(cursor, x, y);
        cursor->x = x;
        cursor->y = y;
    }
        break;
    default:
        break;
    }
}
static lv_obj_t *lv_custom_cursor_label_create(lv_obj_t *parent)
{
    //Write codes screen_label_cursor
    lv_obj_t *label = lv_label_create(parent);
    //lv_obj_set_pos(label, 0, 0);
    lv_obj_clear_flag(label, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_set_size(label, 40, 16);
    lv_obj_set_scrollbar_mode(label, LV_SCROLLBAR_MODE_OFF);
    lv_label_set_text(label, "88.8");
    lv_label_set_long_mode(label, LV_LABEL_LONG_WRAP);

    lv_color_t label_color = lv_color_make(0x00, 0x00, 0x00);//lv_color_make(0xff, 0xc8, 0x00);
    
    //Set style for screen_label_cursor. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
    lv_obj_set_style_radius(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(label, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_color(label, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_dir(label, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_width(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_color(label, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_opa(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_spread(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_ofs_x(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_ofs_y(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_text_color(label, label_color, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(label, &lv_font_montserratMedium_16, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_text_letter_space(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_text_line_space(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_text_align(label, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_pad_left(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_pad_right(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_pad_top(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_pad_bottom(label, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    return label;
}
static lv_obj_t *lv_custom_cursor_line_create(lv_obj_t *parent)
{
    lv_obj_t *obj = lv_obj_create(parent);
    if(obj == NULL)
    {
        return NULL;
    }
    lv_obj_add_flag(obj, LV_OBJ_FLAG_CLICKABLE);
    lv_obj_add_event_cb(obj, custom_cursor_event_handler, LV_EVENT_ALL, obj);
    lv_obj_set_size(obj, LINE_BOARD_WIDTH, LINE_BOARD_WIDTH);
    lv_obj_set_scrollbar_mode(obj, LV_SCROLLBAR_MODE_OFF);

    //Set style for container of cursor. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
    lv_obj_set_style_radius(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(obj, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_color(obj, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_bg_grad_dir(obj, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_width(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_color(obj, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_opa(obj, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_spread(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_ofs_x(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_ofs_y(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_border_color(obj, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(obj, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_pad_left(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_pad_right(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_pad_top(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_pad_bottom(obj, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
    
    //Write codes line_top
    lv_obj_t *line_top = lv_line_create(obj);
    lv_obj_set_pos(line_top, 0, 0);
    lv_obj_set_size(line_top, LINE_BOARD_WIDTH, LINE_BOARD_WIDTH);
    lv_obj_set_scrollbar_mode(line_top, LV_SCROLLBAR_MODE_OFF);
    
    lv_color_t line_color = lv_color_make(0x00, 0x00, 0x00);//lv_color_make(0xff, 0xa3, 0x00);

    //Set style for line_top. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
    lv_obj_set_style_line_color(line_top, line_color, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_line_width(line_top, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_line_rounded(line_top, true, LV_PART_MAIN|LV_STATE_DEFAULT);
    {
        static lv_point_t line_points[] ={{LINE_BOARD_WIDTH/2, LINE_BOARD_WIDTH/2-5},{LINE_BOARD_WIDTH/2, LINE_BOARD_WIDTH/2-3},};
        lv_line_set_points(line_top, line_points, 2);
    }

    //Write codes line_bot
    lv_obj_t *line_bot = lv_line_create(obj);
    lv_obj_set_pos(line_bot, 0, 0);
    lv_obj_set_size(line_bot, LINE_BOARD_WIDTH, LINE_BOARD_WIDTH);
    lv_obj_set_scrollbar_mode(line_bot, LV_SCROLLBAR_MODE_OFF);

    //Set style for line_bot. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
    lv_obj_set_style_line_color(line_bot, line_color, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_line_width(line_bot, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_line_rounded(line_bot, true, LV_PART_MAIN|LV_STATE_DEFAULT);
    {
        static lv_point_t line_points[] ={{LINE_BOARD_WIDTH/2, LINE_BOARD_WIDTH/2+3},{LINE_BOARD_WIDTH/2, LINE_BOARD_WIDTH/2+5},};
        lv_line_set_points(line_bot, line_points, 2);
    }

    //Write codes line_left
    lv_obj_t *line_left = lv_line_create(obj);
    lv_obj_set_pos(line_left, 0, 0);
    lv_obj_set_size(line_left, LINE_BOARD_WIDTH, LINE_BOARD_WIDTH);
    lv_obj_set_scrollbar_mode(line_left, LV_SCROLLBAR_MODE_OFF);

    //Set style for line_left. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
    lv_obj_set_style_line_color(line_left, line_color, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_line_width(line_left, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_line_rounded(line_left, true, LV_PART_MAIN|LV_STATE_DEFAULT);
    {
        static lv_point_t line_points[] ={{LINE_BOARD_WIDTH/2-5, LINE_BOARD_WIDTH/2},{LINE_BOARD_WIDTH/2-3, LINE_BOARD_WIDTH/2},};
        lv_line_set_points(line_left, line_points, 2);
    }

    //Write codes line_right
    lv_obj_t *line_right = lv_line_create(obj);
    lv_obj_set_pos(line_right, 0, 0);
    lv_obj_set_size(line_right, LINE_BOARD_WIDTH, LINE_BOARD_WIDTH);
    lv_obj_set_scrollbar_mode(line_right, LV_SCROLLBAR_MODE_OFF);

    //Set style for line_right. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
    lv_obj_set_style_line_color(line_right, line_color, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_line_width(line_right, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_set_style_line_rounded(line_right, true, LV_PART_MAIN|LV_STATE_DEFAULT);
    {
        static lv_point_t line_points[] ={{LINE_BOARD_WIDTH/2+3, LINE_BOARD_WIDTH/2},{LINE_BOARD_WIDTH/2+5, LINE_BOARD_WIDTH/2},};
        lv_line_set_points(line_right, line_points, 2);
    }

    return obj;
}

static void lv_custom_cursor_set_pos(temp_cursor_object_t *temp_cursor, int16_t x, int16_t y)
{
    uint16_t label_x, label_y;
    uint16_t w, h, label_w;
    lv_obj_t * act_scr = lv_scr_act();
    x -= LINE_BOARD_WIDTH/2;
    y -= LINE_BOARD_WIDTH/2;
    if(act_scr)
    {
        w = lv_obj_get_width(act_scr);
        h = lv_obj_get_height(act_scr);
    }
    else
    {
        w = 320;
        h = 240;
    }
    label_w = lv_obj_get_content_width(temp_cursor->label);
    if(x >= (w - label_w - LINE_BOARD_WIDTH))
    {
        label_x = x - label_w;
        lv_obj_set_style_text_align(temp_cursor->label, LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN|LV_STATE_DEFAULT);
    }
    else
    {
        label_x = x + LINE_BOARD_WIDTH;
        lv_obj_set_style_text_align(temp_cursor->label, LV_TEXT_ALIGN_LEFT, LV_PART_MAIN|LV_STATE_DEFAULT);
    }
    if(y < 0)
    {
        label_y = 0;
    }
    else if(y > (h - 16))
    {
        label_y = h - 16;
    }
    else
    {
        label_y = y;
    }
    lv_obj_set_pos(temp_cursor->cursor, x, y);
    lv_obj_set_pos(temp_cursor->label, label_x, label_y);
    //lv_label_set_text_fmt(guider_ui.screen_label_log, "%d,%d:%d,%d", x, y, label_x, label_y);
}
static void lv_custom_cursor_create(temp_cursor_object_t *temp_cursor, lv_obj_t *parent)
{
    temp_cursor->label = lv_custom_cursor_label_create(parent);
    temp_cursor->cursor = lv_custom_cursor_line_create(parent);
    lv_obj_set_user_data(temp_cursor->cursor, temp_cursor);
}

/**
 * Create a demo application
 */

void custom_init(lv_ui *ui)
{
    /* Add your codes here */
    //lv_obj_add_flag(ui->screen_files_list_img, LV_OBJ_FLAG_HIDDEN);
}

void screen_temp_cursor_create(lv_obj_t *parent)
{
    for(int i=0; i<active_cursor; i++)
    {
        lv_custom_cursor_create(&temp_cursor[i], parent);
        lv_custom_cursor_set_pos(&temp_cursor[i], temp_cursor[i].x, temp_cursor[i].y);
    }
    if(temp_cursor[0].cursor)
    {
        lv_obj_clear_flag(temp_cursor[0].cursor, LV_OBJ_FLAG_CLICKABLE);
    }
}

void screen_temp_cursor_set_num(int32_t num)
{
    int i;
    if(num > 5)
    {
        num = 5;
    }
    if(num < 1)
    {
        num = 1;
    }
    active_cursor = num;
}

int32_t screen_temp_cursor_get_num(void)
{
    return active_cursor;
}

uint16_t lv_custom_temp_cursor_get_pos(struct lv_custom_temp_cursor_data *data, int pos, uint16_t num)
{
    if(pos > active_cursor)
    {
        return 0;
    }
    if(pos < 0)
    {
        temp_cursor->x = data->x;
        temp_cursor->y = data->y;
        num = 1;
    }
    else if(pos < (active_cursor))
    {
        if((pos + num) > active_cursor)
        {
            num = active_cursor - pos;
        }
        for(int i=pos; i<(pos+num); i++)
        {
            data[i].x = temp_cursor[i].x;
            data[i].y = temp_cursor[i].y;
        }
    }
    else
    {
        num = 0;
    }
    
    return num;
}

void lv_custom_temp_cursor_set_pos(struct lv_custom_temp_cursor_data *data, int pos, uint16_t num)
{
    if(pos < 0)
    {
        temp_cursor->x = data->x;
        temp_cursor->y = data->y;
        lv_custom_cursor_set_pos(temp_cursor, data->x, data->y);
    }
    else if(pos < 4)
    {
        if((pos + num) > 5)
        {
            num = 5 - pos;
        }
        for(int i=pos; i<(num+pos); i++)
        {
            temp_cursor[i].x = data[i].x;
            temp_cursor[i].y = data[i].y;
            lv_custom_cursor_set_pos(&temp_cursor[i], data[i].x, data[i].y);
        }
    }
}

void lv_custom_temp_cursor_set_temp(int16_t *data, uint32_t num)
{
    if(num > active_cursor)
    {
        num = active_cursor;
    }
    for(int i=0; i<num; i++)
    {
        int16_t num_int = data[i] / 100;
        int16_t num_dec = (data[i] % 100) / 10;
        if(num_dec < 0)
        {
            num_dec = -num_dec;
        }
        lv_label_set_text_fmt(temp_cursor[i].label, "%d.%d", num_int, num_dec);
    }
}
void screen_btnm_display(lv_ui *ui, bool sta)
{
    #if 0
    if(sta)
    {
        //lv_obj_add_flag(ui->screen_img_iray, LV_OBJ_FLAG_HIDDEN);
        lv_obj_clear_flag(ui->screen_btnm_bot, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
        lv_obj_add_flag(ui->screen_btnm_bot, LV_OBJ_FLAG_HIDDEN);
        //lv_obj_clear_flag(ui->screen_img_iray, LV_OBJ_FLAG_HIDDEN);
    }
    #endif
}

void screen_btnm_display_toggle(lv_ui *ui)
{
    #if 0
    if(lv_obj_has_flag(ui->screen_btnm_bot, LV_OBJ_FLAG_HIDDEN))
    //if(sta)
    {
        //lv_obj_add_flag(ui->screen_img_iray, LV_OBJ_FLAG_HIDDEN);
        lv_obj_clear_flag(ui->screen_btnm_bot, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
        lv_obj_add_flag(ui->screen_btnm_bot, LV_OBJ_FLAG_HIDDEN);
        //lv_obj_clear_flag(ui->screen_img_iray, LV_OBJ_FLAG_HIDDEN);
    }
    #endif
}

void lv_custom_do_capture(void)
{
    void iray_mode_flag_sync_capture(void);
    iray_mode_flag_sync_capture();
}

void lv_custom_do_record(void)
{
    void iray_mode_flag_sync_record(void);
    iray_mode_flag_sync_record();
}

int lv_custom_take_snapshot(void * buf, uint32_t buff_size)
{
    lv_img_dsc_t dsc = {0};
    uint32_t size = lv_snapshot_buf_size_needed(guider_ui.screen_img_iray, LV_IMG_CF_TRUE_COLOR);
    rt_kprintf("buff size:%d, need size:%d\r\n", buff_size, size);
    if(lv_snapshot_take_to_buf(guider_ui.screen_img_iray, LV_IMG_CF_TRUE_COLOR, &dsc, buf, buff_size) == LV_RES_INV)
    {
        return -1;
    }
    return 0;
}

static lv_img_dsc_t *_iray_img_dsc = RT_NULL;
void lv_custom_img_iray_descriptor(lv_img_dsc_t *img_dsc)
{
    _iray_img_dsc = img_dsc;
}

void lv_custom_img_iray_init(lv_ui *ui)
{
    if(ui->screen_img_iray)
    {
        lv_img_set_src(ui->screen_img_iray, _iray_img_dsc);
        lv_img_set_pivot(ui->screen_img_iray, 160, 120);
        lv_img_set_angle(ui->screen_img_iray, 0);
    }
}

void lv_custom_img_iray_update(void)
{
    //lv_custom_img_iray_init(&guider_ui);
    if(guider_ui.screen_img_iray)
    {
        lv_obj_invalidate(guider_ui.screen_img_iray);
    }
}

void lv_custom_img_iray_updated(void)
{
    void iray_img_update_notify(void);
    iray_img_update_notify();
}

int lv_custom_get_current_screen_index(void)
{
    lv_obj_t *screen = lv_scr_act();
    if(screen)
    {
        if(screen == guider_ui.screen)
        {
            return 0;
        }
        else if(screen == guider_ui.screen_files)
        {
            return 1;
        }
        else if(screen == guider_ui.screen_settings)
        {
            return 2;
        }
    }
    
    return -1;
}

static lv_style_t style_screen_files_list_img_extra_btns_main_default;
void lv_custom_files_list_style_init(void)
{
    //Set style state: LV_STATE_DEFAULT for style_screen_files_list_img_extra_texts_main_default
    ui_init_style(&style_screen_files_list_img_extra_btns_main_default);
    lv_style_set_radius(&style_screen_files_list_img_extra_btns_main_default, 3);
    lv_style_set_bg_color(&style_screen_files_list_img_extra_btns_main_default, lv_color_make(0xff, 0xff, 0xff));
    lv_style_set_bg_grad_color(&style_screen_files_list_img_extra_btns_main_default, lv_color_make(0xff, 0xff, 0xff));
    lv_style_set_bg_grad_dir(&style_screen_files_list_img_extra_btns_main_default, LV_GRAD_DIR_NONE);
    lv_style_set_bg_opa(&style_screen_files_list_img_extra_btns_main_default, 255);
    lv_style_set_text_color(&style_screen_files_list_img_extra_btns_main_default, lv_color_make(0x0D, 0x30, 0x55));
    lv_style_set_text_font(&style_screen_files_list_img_extra_btns_main_default, &lv_font_montserratMedium_12);
}

void lv_custom_files_list_clear(void)
{
    for(int i = 1; i < lv_obj_get_child_cnt(guider_ui.screen_files_list_img); i++) {
        lv_obj_t * child = lv_obj_get_child(guider_ui.screen_files_list_img, i);
        if(lv_obj_check_type(child, &lv_list_btn_class)) {
            lv_obj_del(child);
        }
    }
}

static void screen_files_list_img_item_event_handler(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    switch (code)
    {
    case LV_EVENT_CLICKED:
    {
        lv_obj_t *btn = lv_event_get_target(e);
        if(btn)
        {
            const char *file = lv_list_get_btn_text(NULL, btn);
            rt_kprintf("open file:%s\r\n", file);
            void iray_load_picture(const char *fname, void *buff);
            iray_load_picture(file, (void *)_iray_img_dsc->data);
            lv_custom_files_img_update();
        }
    }
        break;
    default:
        break;
    }
}
void lv_custom_files_list_append(const char *file)
{
    lv_obj_t *screen_files_list_img_item;
    screen_files_list_img_item = lv_list_add_btn(guider_ui.screen_files_list_img, LV_SYMBOL_IMAGE, file);
    lv_obj_add_style(screen_files_list_img_item, &style_screen_files_list_img_extra_btns_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
    lv_obj_add_event_cb(screen_files_list_img_item, screen_files_list_img_item_event_handler, LV_EVENT_ALL, NULL);
}
void lv_custom_files_list_update(void)
{
    void update_file_list();
    lv_custom_files_list_clear();
    update_file_list();
}
void lv_custom_files_img_update(void)
{
    lv_obj_t *img = guider_ui.screen_files_img_files;
    if(guider_ui.screen_files)
    {
        lv_img_set_src(img, _iray_img_dsc);
        lv_img_set_pivot(img, 160, 120);
        lv_img_set_angle(img, 0);
    }
}

