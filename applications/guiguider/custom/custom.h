// SPDX-License-Identifier: MIT
// Copyright 2020 NXP

/*
 * custom.h
 *
 *  Created on: July 29, 2020
 *      Author: nxf53801
 */

#ifndef __CUSTOM_H_
#define __CUSTOM_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "gui_guider.h"

struct lv_custom_temp_cursor_data
{
    uint16_t x;
    uint16_t y;
};

void custom_init(lv_ui *ui);
void screen_btnm_display(lv_ui *ui, bool sta);
void screen_btnm_display_toggle(lv_ui *ui);

void screen_temp_cursor_create(lv_obj_t *parent);
void screen_temp_cursor_set_num(int32_t num);
int32_t screen_temp_cursor_get_num(void);
uint16_t lv_custom_temp_cursor_get_pos(struct lv_custom_temp_cursor_data *data, int pos, uint16_t num);
void lv_custom_temp_cursor_set_pos(struct lv_custom_temp_cursor_data *data, int pos, uint16_t num);
void lv_custom_temp_cursor_set_temp(int16_t *data, uint32_t num);

void lv_custom_do_capture(void);
void lv_custom_do_record(void);
int lv_custom_take_snapshot(void * buf, uint32_t buff_size);

void lv_custom_img_iray_descriptor(lv_img_dsc_t *img_dsc);
void lv_custom_img_iray_init(lv_ui *ui);
void lv_custom_img_iray_update(void);
void lv_custom_img_iray_updated(void);

int lv_custom_get_current_screen_index(void);

void lv_custom_files_list_style_init(void);
void lv_custom_files_list_clear(void);
void lv_custom_files_list_append(const char *file);
void lv_custom_files_list_update(void);
void lv_custom_files_img_update(void);

#ifdef __cplusplus
}
#endif
#endif /* EVENT_CB_H_ */
