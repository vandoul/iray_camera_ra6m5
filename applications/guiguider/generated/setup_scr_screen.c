/*
 * Copyright 2023 NXP
 * SPDX-License-Identifier: MIT
 * The auto-generated can only be used on NXP devices
 */

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "custom.h"


void setup_scr_screen(lv_ui *ui){

	//if(ui->screen)
	//{
	//	lv_obj_del(ui->screen);
	//}
	//Write codes screen
	ui->screen = lv_obj_create(NULL);
	lv_obj_set_scrollbar_mode(ui->screen, LV_SCROLLBAR_MODE_OFF);

	//Set style for screen. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_bg_color(ui->screen, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->screen, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->screen, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_line_NULL
	ui->screen_line_NULL = lv_line_create(ui->screen);
	lv_obj_set_pos(ui->screen_line_NULL, 0, 0);
	lv_obj_set_size(ui->screen_line_NULL, 1, 1);
	lv_obj_set_scrollbar_mode(ui->screen_line_NULL, LV_SCROLLBAR_MODE_OFF);

	//Set style for screen_line_NULL. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_line_color(ui->screen_line_NULL, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_line_width(ui->screen_line_NULL, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_line_rounded(ui->screen_line_NULL, true, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Hidden for widget screen_line_NULL
	lv_obj_add_flag(ui->screen_line_NULL, LV_OBJ_FLAG_HIDDEN);

	static lv_point_t screen_line_NULL[] ={{0, 0},{1, 1},};
	lv_line_set_points(ui->screen_line_NULL,screen_line_NULL,2);

	//Write codes screen_img_iray
	ui->screen_img_iray = lv_img_create(ui->screen);
	lv_obj_set_pos(ui->screen_img_iray, 20, 0);
	lv_obj_set_size(ui->screen_img_iray, 280, 210);
	lv_obj_set_scrollbar_mode(ui->screen_img_iray, LV_SCROLLBAR_MODE_OFF);

	//Set style for screen_img_iray. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_img_recolor(ui->screen_img_iray, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor_opa(ui->screen_img_iray, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->screen_img_iray, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_btnm_bot
	ui->screen_btnm_bot = lv_btnmatrix_create(ui->screen);
	lv_obj_set_pos(ui->screen_btnm_bot, 0, 210);
	lv_obj_set_size(ui->screen_btnm_bot, 320, 30);
	lv_obj_set_scrollbar_mode(ui->screen_btnm_bot, LV_SCROLLBAR_MODE_OFF);
	static const char *screen_btnm_bot_text_map[] = {
	"SETTING","FILE","REC","CAP",
	"",};
	lv_btnmatrix_set_map(ui->screen_btnm_bot, screen_btnm_bot_text_map);

	//Set style for screen_btnm_bot. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->screen_btnm_bot, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_btnm_bot, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->screen_btnm_bot, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->screen_btnm_bot, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_btnm_bot, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_btnm_bot, lv_color_make(0xc9, 0xc9, 0xc9), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_btnm_bot, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_btnm_bot, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_btnm_bot, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_btnm_bot, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_btnm_bot, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_btnm_bot, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_row(ui->screen_btnm_bot, 8, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_column(ui->screen_btnm_bot, 8, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Set style for screen_btnm_bot. Part: LV_PART_ITEMS, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->screen_btnm_bot, 4, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_btnm_bot, lv_color_make(0x21, 0x95, 0xf6), LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->screen_btnm_bot, lv_color_make(0x21, 0x95, 0xf6), LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->screen_btnm_bot, LV_GRAD_DIR_NONE, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_btnm_bot, 255, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_btnm_bot, 0, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->screen_btnm_bot, lv_color_make(0x21, 0x95, 0xf6), LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->screen_btnm_bot, 255, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->screen_btnm_bot, 0, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->screen_btnm_bot, 0, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->screen_btnm_bot, 0, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_btnm_bot, lv_color_make(0xc9, 0xc9, 0xc9), LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_btnm_bot, 1, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_btnm_bot, 255, LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_text_color(ui->screen_btnm_bot, lv_color_make(0xff, 0xff, 0xff), LV_PART_ITEMS|LV_STATE_DEFAULT);
	lv_obj_set_style_text_font(ui->screen_btnm_bot, &lv_font_montserratMedium_16, LV_PART_ITEMS|LV_STATE_DEFAULT);

	//Init events for screen
	events_init_screen(ui);
}