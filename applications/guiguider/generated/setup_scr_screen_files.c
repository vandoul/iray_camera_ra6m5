/*
 * Copyright 2023 NXP
 * SPDX-License-Identifier: MIT
 * The auto-generated can only be used on NXP devices
 */

#include "lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "custom.h"


void setup_scr_screen_files(lv_ui *ui){

	if(ui->screen_files)
	{
		//lv_obj_del(ui->screen_files);
	}
	//Write codes screen_files
	ui->screen_files = lv_obj_create(NULL);
	lv_obj_set_scrollbar_mode(ui->screen_files, LV_SCROLLBAR_MODE_OFF);

	//Set style for screen_files. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_bg_color(ui->screen_files, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->screen_files, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->screen_files, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_files, 0, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_files_img_files
	ui->screen_files_img_files = lv_img_create(ui->screen_files);
	lv_obj_set_pos(ui->screen_files_img_files, 0, 0);
	lv_obj_set_size(ui->screen_files_img_files, 320, 240);
	lv_obj_set_scrollbar_mode(ui->screen_files_img_files, LV_SCROLLBAR_MODE_OFF);

	//Set style for screen_files_img_files. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_img_recolor(ui->screen_files_img_files, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_recolor_opa(ui->screen_files_img_files, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_img_opa(ui->screen_files_img_files, 255, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Write codes screen_files_list_img
	ui->screen_files_list_img = lv_list_create(ui->screen_files);
	lv_obj_set_pos(ui->screen_files_list_img, 160, 20);
	lv_obj_set_size(ui->screen_files_list_img, 160, 200);
	lv_obj_set_scrollbar_mode(ui->screen_files_list_img, LV_SCROLLBAR_MODE_AUTO);

	//Set style for screen_files_list_img. Part: LV_PART_MAIN, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->screen_files_list_img, 3, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_files_list_img, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_color(ui->screen_files_list_img, lv_color_make(0xff, 0xff, 0xff), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_grad_dir(ui->screen_files_list_img, LV_GRAD_DIR_NONE, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_files_list_img, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_width(ui->screen_files_list_img, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_color(ui->screen_files_list_img, lv_color_make(0x21, 0x95, 0xf6), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_opa(ui->screen_files_list_img, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_spread(ui->screen_files_list_img, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_x(ui->screen_files_list_img, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_shadow_ofs_y(ui->screen_files_list_img, 0, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_color(ui->screen_files_list_img, lv_color_make(0xe1, 0xe6, 0xee), LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_width(ui->screen_files_list_img, 1, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_border_opa(ui->screen_files_list_img, 255, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_left(ui->screen_files_list_img, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_right(ui->screen_files_list_img, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_top(ui->screen_files_list_img, 5, LV_PART_MAIN|LV_STATE_DEFAULT);
	lv_obj_set_style_pad_bottom(ui->screen_files_list_img, 5, LV_PART_MAIN|LV_STATE_DEFAULT);

	//Set style for screen_files_list_img. Part: LV_PART_SCROLLBAR, State: LV_STATE_DEFAULT
	lv_obj_set_style_radius(ui->screen_files_list_img, 3, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_color(ui->screen_files_list_img, lv_color_make(0xff, 0xff, 0xff), LV_PART_SCROLLBAR|LV_STATE_DEFAULT);
	lv_obj_set_style_bg_opa(ui->screen_files_list_img, 255, LV_PART_SCROLLBAR|LV_STATE_DEFAULT);

	//Set style state: LV_STATE_DEFAULT for style_screen_files_list_img_extra_btns_main_default
	static lv_style_t style_screen_files_list_img_extra_btns_main_default;
	ui_init_style(&style_screen_files_list_img_extra_btns_main_default);
	lv_style_set_radius(&style_screen_files_list_img_extra_btns_main_default, 3);
	lv_style_set_bg_color(&style_screen_files_list_img_extra_btns_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_color(&style_screen_files_list_img_extra_btns_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_dir(&style_screen_files_list_img_extra_btns_main_default, LV_GRAD_DIR_NONE);
	lv_style_set_bg_opa(&style_screen_files_list_img_extra_btns_main_default, 255);
	lv_style_set_text_color(&style_screen_files_list_img_extra_btns_main_default, lv_color_make(0x0D, 0x30, 0x55));
	lv_style_set_text_font(&style_screen_files_list_img_extra_btns_main_default, &lv_font_montserratMedium_12);

	//Set style state: LV_STATE_DEFAULT for style_screen_files_list_img_extra_texts_main_default
	static lv_style_t style_screen_files_list_img_extra_texts_main_default;
	ui_init_style(&style_screen_files_list_img_extra_texts_main_default);
	lv_style_set_radius(&style_screen_files_list_img_extra_texts_main_default, 3);
	lv_style_set_bg_color(&style_screen_files_list_img_extra_texts_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_color(&style_screen_files_list_img_extra_texts_main_default, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_dir(&style_screen_files_list_img_extra_texts_main_default, LV_GRAD_DIR_NONE);
	lv_style_set_bg_opa(&style_screen_files_list_img_extra_texts_main_default, 255);
	lv_style_set_text_color(&style_screen_files_list_img_extra_texts_main_default, lv_color_make(0x0D, 0x30, 0x55));
	lv_style_set_text_font(&style_screen_files_list_img_extra_texts_main_default, &lv_font_montserratMedium_12);

	lv_obj_t *screen_files_list_img_item;
	screen_files_list_img_item = lv_list_add_btn(ui->screen_files_list_img, LV_SYMBOL_LEFT, "..");
	ui->screen_files_list_img_item0 = screen_files_list_img_item;
	lv_obj_add_style(screen_files_list_img_item, &style_screen_files_list_img_extra_btns_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
#if 0
	screen_files_list_img_item = lv_list_add_btn(ui->screen_files_list_img, LV_SYMBOL_IMAGE, "1.bmp");
	ui->screen_files_list_img_itemN = screen_files_list_img_item;
	lv_obj_add_style(screen_files_list_img_item, &style_screen_files_list_img_extra_btns_main_default, LV_PART_MAIN|LV_STATE_DEFAULT);
#endif
	//Init events for screen
	events_init_screen_files(ui);
}