# Copyright 2023 NXP
# SPDX-License-Identifier: MIT
# The auto-generated can only be used on NXP devices

import SDL
import utime as time
import usys as sys
import lvgl as lv
import lodepng as png
import ustruct

lv.init()
SDL.init(w=320,h=240)

# Register SDL display driver.
disp_buf1 = lv.disp_draw_buf_t()
buf1_1 = bytearray(320*10)
disp_buf1.init(buf1_1, None, len(buf1_1)//4)
disp_drv = lv.disp_drv_t()
disp_drv.init()
disp_drv.draw_buf = disp_buf1
disp_drv.flush_cb = SDL.monitor_flush
disp_drv.hor_res = 320
disp_drv.ver_res = 240
disp_drv.register()

# Regsiter SDL mouse driver
indev_drv = lv.indev_drv_t()
indev_drv.init() 
indev_drv.type = lv.INDEV_TYPE.POINTER
indev_drv.read_cb = SDL.mouse_read
indev_drv.register()

# Below: Taken from https://github.com/lvgl/lv_binding_micropython/blob/master/driver/js/imagetools.py#L22-L94

COLOR_SIZE = lv.color_t.__SIZE__
COLOR_IS_SWAPPED = hasattr(lv.color_t().ch,'green_h')

class lodepng_error(RuntimeError):
    def __init__(self, err):
        if type(err) is int:
            super().__init__(png.error_text(err))
        else:
            super().__init__(err)

# Parse PNG file header
# Taken from https://github.com/shibukawa/imagesize_py/blob/ffef30c1a4715c5acf90e8945ceb77f4a2ed2d45/imagesize.py#L63-L85

def get_png_info(decoder, src, header):
    # Only handle variable image types

    if lv.img.src_get_type(src) != lv.img.SRC.VARIABLE:
        return lv.RES.INV

    data = lv.img_dsc_t.__cast__(src).data
    if data == None:
        return lv.RES.INV

    png_header = bytes(data.__dereference__(24))

    if png_header.startswith(b'\211PNG\r\n\032\n'):
        if png_header[12:16] == b'IHDR':
            start = 16
        # Maybe this is for an older PNG version.
        else:
            start = 8
        try:
            width, height = ustruct.unpack(">LL", png_header[start:start+8])
        except ustruct.error:
            return lv.RES.INV
    else:
        return lv.RES.INV

    header.always_zero = 0
    header.w = width
    header.h = height
    header.cf = lv.img.CF.TRUE_COLOR_ALPHA

    return lv.RES.OK

def convert_rgba8888_to_bgra8888(img_view):
    for i in range(0, len(img_view), lv.color_t.__SIZE__):
        ch = lv.color_t.__cast__(img_view[i:i]).ch
        ch.red, ch.blue = ch.blue, ch.red

# Read and parse PNG file

def open_png(decoder, dsc):
    img_dsc = lv.img_dsc_t.__cast__(dsc.src)
    png_data = img_dsc.data
    png_size = img_dsc.data_size
    png_decoded = png.C_Pointer()
    png_width = png.C_Pointer()
    png_height = png.C_Pointer()
    error = png.decode32(png_decoded, png_width, png_height, png_data, png_size)
    if error:
        raise lodepng_error(error)
    img_size = png_width.int_val * png_height.int_val * 4
    img_data = png_decoded.ptr_val
    img_view = img_data.__dereference__(img_size)

    if COLOR_SIZE == 4:
        convert_rgba8888_to_bgra8888(img_view)
    else:
        raise lodepng_error("Error: Color mode not supported yet!")

    dsc.img_data = img_data
    return lv.RES.OK

# Above: Taken from https://github.com/lvgl/lv_binding_micropython/blob/master/driver/js/imagetools.py#L22-L94

decoder = lv.img.decoder_create()
decoder.info_cb = get_png_info
decoder.open_cb = open_png

def anim_x_cb(obj, v):
    obj.set_x(v)

def anim_y_cb(obj, v):
    obj.set_y(v)

def ta_event_cb(e,kb):
    code = e.get_code()
    ta = e.get_target()
    if code == lv.EVENT.FOCUSED:
        kb.set_textarea(ta)
        kb.move_foreground()
        kb.clear_flag(lv.obj.FLAG.HIDDEN)

    if code == lv.EVENT.DEFOCUSED:
        kb.set_textarea(None)
        kb.move_background()
        kb.add_flag(lv.obj.FLAG.HIDDEN)
        
def ta_zh_event_cb(e,kb):
    code = e.get_code()
    ta = e.get_target()
    if code == lv.EVENT.FOCUSED:
        kb.keyboard_set_textarea(ta)
        kb.move_foreground()
        kb.clear_flag(lv.obj.FLAG.HIDDEN)

    if code == lv.EVENT.DEFOCUSED:
        kb.keyboard_set_textarea(None)
        kb.move_background()
        kb.add_flag(lv.obj.FLAG.HIDDEN)



# create screen
screen = lv.obj()
screen.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# create style style_screen_main_main_default
style_screen_main_main_default = lv.style_t()
style_screen_main_main_default.init()
style_screen_main_main_default.set_bg_color(lv.color_make(0xff,0xff,0xff))
style_screen_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_main_main_default.set_bg_opa(255)

# add style for screen
screen.add_style(style_screen_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create screen_img_iray
screen_img_iray = lv.img(screen)
screen_img_iray.set_pos(int(0),int(0))
screen_img_iray.set_size(320,240)
screen_img_iray.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# create style style_screen_img_iray_main_main_default
style_screen_img_iray_main_main_default = lv.style_t()
style_screen_img_iray_main_main_default.init()
style_screen_img_iray_main_main_default.set_img_recolor(lv.color_make(0xff,0xff,0xff))
style_screen_img_iray_main_main_default.set_img_recolor_opa(0)
style_screen_img_iray_main_main_default.set_img_opa(255)

# add style for screen_img_iray
screen_img_iray.add_style(style_screen_img_iray_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create screen_btnm_bot
screen_btnm_bot = lv.btnmatrix(screen)
screen_btnm_bot.set_pos(int(0),int(210))
screen_btnm_bot.set_size(320,30)
screen_btnm_bot.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
btnm_map = [
"SETTING",
"FILE",
"REC",
"CAP",
"",
""]
screen_btnm_bot.set_map(btnm_map)
# create style style_screen_btnm_bot_main_main_default
style_screen_btnm_bot_main_main_default = lv.style_t()
style_screen_btnm_bot_main_main_default.init()
style_screen_btnm_bot_main_main_default.set_radius(0)
style_screen_btnm_bot_main_main_default.set_bg_color(lv.color_make(0xff,0xff,0xff))
style_screen_btnm_bot_main_main_default.set_bg_grad_color(lv.color_make(0xff,0xff,0xff))
style_screen_btnm_bot_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_btnm_bot_main_main_default.set_bg_opa(0)
style_screen_btnm_bot_main_main_default.set_border_color(lv.color_make(0xc9,0xc9,0xc9))
style_screen_btnm_bot_main_main_default.set_border_width(0)
style_screen_btnm_bot_main_main_default.set_border_opa(255)
style_screen_btnm_bot_main_main_default.set_pad_left(0)
style_screen_btnm_bot_main_main_default.set_pad_right(0)
style_screen_btnm_bot_main_main_default.set_pad_top(0)
style_screen_btnm_bot_main_main_default.set_pad_bottom(0)
style_screen_btnm_bot_main_main_default.set_pad_row(8)
style_screen_btnm_bot_main_main_default.set_pad_column(8)

# add style for screen_btnm_bot
screen_btnm_bot.add_style(style_screen_btnm_bot_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

# create style style_screen_btnm_bot_main_items_default
style_screen_btnm_bot_main_items_default = lv.style_t()
style_screen_btnm_bot_main_items_default.init()
style_screen_btnm_bot_main_items_default.set_radius(4)
style_screen_btnm_bot_main_items_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_screen_btnm_bot_main_items_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_btnm_bot_main_items_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_btnm_bot_main_items_default.set_bg_opa(255)
style_screen_btnm_bot_main_items_default.set_border_color(lv.color_make(0xc9,0xc9,0xc9))
style_screen_btnm_bot_main_items_default.set_border_width(1)
style_screen_btnm_bot_main_items_default.set_border_opa(255)
style_screen_btnm_bot_main_items_default.set_text_color(lv.color_make(0xff,0xff,0xff))
try:
    style_screen_btnm_bot_main_items_default.set_text_font(lv.font_montserratMedium_16)
except AttributeError:
    try:
        style_screen_btnm_bot_main_items_default.set_text_font(lv.font_montserrat_16)
    except AttributeError:
        style_screen_btnm_bot_main_items_default.set_text_font(lv.font_montserrat_16)

# add style for screen_btnm_bot
screen_btnm_bot.add_style(style_screen_btnm_bot_main_items_default, lv.PART.ITEMS|lv.STATE.DEFAULT)


# create screen_files
screen_files = lv.obj()
screen_files.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# create style style_screen_files_main_main_default
style_screen_files_main_main_default = lv.style_t()
style_screen_files_main_main_default.init()
style_screen_files_main_main_default.set_bg_color(lv.color_make(0xff,0xff,0xff))
style_screen_files_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_files_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_files_main_main_default.set_bg_opa(0)

# add style for screen_files
screen_files.add_style(style_screen_files_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create screen_files_img_files
screen_files_img_files = lv.img(screen_files)
screen_files_img_files.set_pos(int(0),int(0))
screen_files_img_files.set_size(320,240)
screen_files_img_files.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# create style style_screen_files_img_files_main_main_default
style_screen_files_img_files_main_main_default = lv.style_t()
style_screen_files_img_files_main_main_default.init()
style_screen_files_img_files_main_main_default.set_img_recolor(lv.color_make(0xff,0xff,0xff))
style_screen_files_img_files_main_main_default.set_img_recolor_opa(0)
style_screen_files_img_files_main_main_default.set_img_opa(255)

# add style for screen_files_img_files
screen_files_img_files.add_style(style_screen_files_img_files_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create screen_files_list_img
screen_files_list_img = lv.list(screen_files)
screen_files_list_img.set_pos(int(160),int(20))
screen_files_list_img.set_size(160,200)
screen_files_list_img.set_scrollbar_mode(lv.SCROLLBAR_MODE.AUTO)
# create style style_screen_files_list_img_extra_btns_main_default
style_screen_files_list_img_extra_btns_main_default = lv.style_t()
style_screen_files_list_img_extra_btns_main_default.init()
style_screen_files_list_img_extra_btns_main_default.set_radius(3)
style_screen_files_list_img_extra_btns_main_default.set_bg_color(lv.color_make(0xff,0xff,0xff))
style_screen_files_list_img_extra_btns_main_default.set_bg_grad_color(lv.color_make(0xff,0xff,0xff))
style_screen_files_list_img_extra_btns_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_files_list_img_extra_btns_main_default.set_bg_opa(255)
style_screen_files_list_img_extra_btns_main_default.set_text_color(lv.color_make(0x0D,0x30,0x55))
try:
    style_screen_files_list_img_extra_btns_main_default.set_text_font(lv.font_montserratMedium_12)
except AttributeError:
    try:
        style_screen_files_list_img_extra_btns_main_default.set_text_font(lv.font_montserrat_12)
    except AttributeError:
        style_screen_files_list_img_extra_btns_main_default.set_text_font(lv.font_montserrat_16)


screen_files_list_img_btn_0 = screen_files_list_img.add_btn(lv.SYMBOL.LEFT, "..")

# add style for screen_files_list_img_btn_0
screen_files_list_img_btn_0.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

screen_files_list_img_btn_1 = screen_files_list_img.add_btn(lv.SYMBOL.IMAGE, "1.bmp")

# add style for screen_files_list_img_btn_1
screen_files_list_img_btn_1.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

screen_files_list_img_btn_2 = screen_files_list_img.add_btn(lv.SYMBOL.IMAGE, "2.bmp")

# add style for screen_files_list_img_btn_2
screen_files_list_img_btn_2.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

screen_files_list_img_btn_3 = screen_files_list_img.add_btn(lv.SYMBOL.IMAGE, "3.bmp")

# add style for screen_files_list_img_btn_3
screen_files_list_img_btn_3.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

screen_files_list_img_btn_4 = screen_files_list_img.add_btn(lv.SYMBOL.IMAGE, "4.bmp")

# add style for screen_files_list_img_btn_4
screen_files_list_img_btn_4.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

screen_files_list_img_btn_5 = screen_files_list_img.add_btn(lv.SYMBOL.IMAGE, "5.bmp")

# add style for screen_files_list_img_btn_5
screen_files_list_img_btn_5.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

screen_files_list_img_btn_6 = screen_files_list_img.add_btn(lv.SYMBOL.IMAGE, "6.bmp")

# add style for screen_files_list_img_btn_6
screen_files_list_img_btn_6.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

screen_files_list_img_btn_7 = screen_files_list_img.add_btn(lv.SYMBOL.IMAGE, "7.bmp")

# add style for screen_files_list_img_btn_7
screen_files_list_img_btn_7.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

screen_files_list_img_btn_8 = screen_files_list_img.add_btn(lv.SYMBOL.IMAGE, "8.bmp")

# add style for screen_files_list_img_btn_8
screen_files_list_img_btn_8.add_style(style_screen_files_list_img_extra_btns_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

# create style style_screen_files_list_img_main_main_default
style_screen_files_list_img_main_main_default = lv.style_t()
style_screen_files_list_img_main_main_default.init()
style_screen_files_list_img_main_main_default.set_radius(3)
style_screen_files_list_img_main_main_default.set_bg_color(lv.color_make(0xff,0xff,0xff))
style_screen_files_list_img_main_main_default.set_bg_grad_color(lv.color_make(0xff,0xff,0xff))
style_screen_files_list_img_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_files_list_img_main_main_default.set_bg_opa(255)
style_screen_files_list_img_main_main_default.set_border_color(lv.color_make(0xe1,0xe6,0xee))
style_screen_files_list_img_main_main_default.set_border_width(1)
style_screen_files_list_img_main_main_default.set_border_opa(255)
style_screen_files_list_img_main_main_default.set_pad_left(5)
style_screen_files_list_img_main_main_default.set_pad_right(5)
style_screen_files_list_img_main_main_default.set_pad_top(5)
style_screen_files_list_img_main_main_default.set_pad_bottom(5)

# add style for screen_files_list_img
screen_files_list_img.add_style(style_screen_files_list_img_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

# create style style_screen_files_list_img_main_scrollbar_default
style_screen_files_list_img_main_scrollbar_default = lv.style_t()
style_screen_files_list_img_main_scrollbar_default.init()
style_screen_files_list_img_main_scrollbar_default.set_radius(3)
style_screen_files_list_img_main_scrollbar_default.set_bg_color(lv.color_make(0xff,0xff,0xff))
style_screen_files_list_img_main_scrollbar_default.set_bg_opa(255)

# add style for screen_files_list_img
screen_files_list_img.add_style(style_screen_files_list_img_main_scrollbar_default, lv.PART.SCROLLBAR|lv.STATE.DEFAULT)


# create screen_settings
screen_settings = lv.obj()
screen_settings.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
# create style style_screen_settings_main_main_default
style_screen_settings_main_main_default = lv.style_t()
style_screen_settings_main_main_default.init()
style_screen_settings_main_main_default.set_bg_color(lv.color_make(0xff,0xff,0xff))
style_screen_settings_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_settings_main_main_default.set_bg_opa(0)

# add style for screen_settings
screen_settings.add_style(style_screen_settings_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create screen_settings_slider_num
screen_settings_slider_num = lv.slider(screen_settings)
screen_settings_slider_num.set_pos(int(93),int(116))
screen_settings_slider_num.set_size(160,8)
screen_settings_slider_num.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
screen_settings_slider_num.set_range(0, 5)
screen_settings_slider_num.set_value(1, False)

# create style style_screen_settings_slider_num_main_main_default
style_screen_settings_slider_num_main_main_default = lv.style_t()
style_screen_settings_slider_num_main_main_default.init()
style_screen_settings_slider_num_main_main_default.set_radius(50)
style_screen_settings_slider_num_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_slider_num_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_slider_num_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_settings_slider_num_main_main_default.set_bg_opa(60)
style_screen_settings_slider_num_main_main_default.set_outline_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_slider_num_main_main_default.set_outline_width(0)
style_screen_settings_slider_num_main_main_default.set_outline_opa(0)

# add style for screen_settings_slider_num
screen_settings_slider_num.add_style(style_screen_settings_slider_num_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)

# create style style_screen_settings_slider_num_main_indicator_default
style_screen_settings_slider_num_main_indicator_default = lv.style_t()
style_screen_settings_slider_num_main_indicator_default.init()
style_screen_settings_slider_num_main_indicator_default.set_radius(50)
style_screen_settings_slider_num_main_indicator_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_slider_num_main_indicator_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_slider_num_main_indicator_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_settings_slider_num_main_indicator_default.set_bg_opa(255)

# add style for screen_settings_slider_num
screen_settings_slider_num.add_style(style_screen_settings_slider_num_main_indicator_default, lv.PART.INDICATOR|lv.STATE.DEFAULT)

# create style style_screen_settings_slider_num_main_knob_default
style_screen_settings_slider_num_main_knob_default = lv.style_t()
style_screen_settings_slider_num_main_knob_default.init()
style_screen_settings_slider_num_main_knob_default.set_radius(50)
style_screen_settings_slider_num_main_knob_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_slider_num_main_knob_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_slider_num_main_knob_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_settings_slider_num_main_knob_default.set_bg_opa(255)

# add style for screen_settings_slider_num
screen_settings_slider_num.add_style(style_screen_settings_slider_num_main_knob_default, lv.PART.KNOB|lv.STATE.DEFAULT)


# create screen_settings_label_value
screen_settings_label_value = lv.label(screen_settings)
screen_settings_label_value.set_pos(int(279),int(112))
screen_settings_label_value.set_size(16,16)
screen_settings_label_value.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
screen_settings_label_value.set_text("1")
screen_settings_label_value.set_long_mode(lv.label.LONG.WRAP)
# create style style_screen_settings_label_value_main_main_default
style_screen_settings_label_value_main_main_default = lv.style_t()
style_screen_settings_label_value_main_main_default.init()
style_screen_settings_label_value_main_main_default.set_radius(0)
style_screen_settings_label_value_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_label_value_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_label_value_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_settings_label_value_main_main_default.set_bg_opa(255)
style_screen_settings_label_value_main_main_default.set_text_color(lv.color_make(0xff,0xff,0xff))
try:
    style_screen_settings_label_value_main_main_default.set_text_font(lv.font_montserratMedium_16)
except AttributeError:
    try:
        style_screen_settings_label_value_main_main_default.set_text_font(lv.font_montserrat_16)
    except AttributeError:
        style_screen_settings_label_value_main_main_default.set_text_font(lv.font_montserrat_16)
style_screen_settings_label_value_main_main_default.set_text_letter_space(0)
style_screen_settings_label_value_main_main_default.set_text_line_space(0)
style_screen_settings_label_value_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_screen_settings_label_value_main_main_default.set_pad_left(0)
style_screen_settings_label_value_main_main_default.set_pad_right(0)
style_screen_settings_label_value_main_main_default.set_pad_top(0)
style_screen_settings_label_value_main_main_default.set_pad_bottom(0)

# add style for screen_settings_label_value
screen_settings_label_value.add_style(style_screen_settings_label_value_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create screen_settings_label_num
screen_settings_label_num = lv.label(screen_settings)
screen_settings_label_num.set_pos(int(21),int(112))
screen_settings_label_num.set_size(48,16)
screen_settings_label_num.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
screen_settings_label_num.set_text("num")
screen_settings_label_num.set_long_mode(lv.label.LONG.WRAP)
# create style style_screen_settings_label_num_main_main_default
style_screen_settings_label_num_main_main_default = lv.style_t()
style_screen_settings_label_num_main_main_default.init()
style_screen_settings_label_num_main_main_default.set_radius(0)
style_screen_settings_label_num_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_label_num_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_label_num_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_settings_label_num_main_main_default.set_bg_opa(255)
style_screen_settings_label_num_main_main_default.set_text_color(lv.color_make(0xff,0xff,0xff))
try:
    style_screen_settings_label_num_main_main_default.set_text_font(lv.font_montserratMedium_16)
except AttributeError:
    try:
        style_screen_settings_label_num_main_main_default.set_text_font(lv.font_montserrat_16)
    except AttributeError:
        style_screen_settings_label_num_main_main_default.set_text_font(lv.font_montserrat_16)
style_screen_settings_label_num_main_main_default.set_text_letter_space(0)
style_screen_settings_label_num_main_main_default.set_text_line_space(0)
style_screen_settings_label_num_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)
style_screen_settings_label_num_main_main_default.set_pad_left(0)
style_screen_settings_label_num_main_main_default.set_pad_right(0)
style_screen_settings_label_num_main_main_default.set_pad_top(0)
style_screen_settings_label_num_main_main_default.set_pad_bottom(0)

# add style for screen_settings_label_num
screen_settings_label_num.add_style(style_screen_settings_label_num_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)


# create screen_settings_btn_s_back
screen_settings_btn_s_back = lv.btn(screen_settings)
screen_settings_btn_s_back.set_pos(int(279),int(215))
screen_settings_btn_s_back.set_size(32,16)
screen_settings_btn_s_back.set_scrollbar_mode(lv.SCROLLBAR_MODE.OFF)
screen_settings_btn_s_back_label = lv.label(screen_settings_btn_s_back)
screen_settings_btn_s_back_label.set_text("<")
screen_settings_btn_s_back.set_style_pad_all(0, lv.STATE.DEFAULT)
screen_settings_btn_s_back_label.align(lv.ALIGN.CENTER,0,0)
# create style style_screen_settings_btn_s_back_main_main_default
style_screen_settings_btn_s_back_main_main_default = lv.style_t()
style_screen_settings_btn_s_back_main_main_default.init()
style_screen_settings_btn_s_back_main_main_default.set_radius(0)
style_screen_settings_btn_s_back_main_main_default.set_bg_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_btn_s_back_main_main_default.set_bg_grad_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_btn_s_back_main_main_default.set_bg_grad_dir(lv.GRAD_DIR.NONE)
style_screen_settings_btn_s_back_main_main_default.set_bg_opa(255)
style_screen_settings_btn_s_back_main_main_default.set_border_color(lv.color_make(0x21,0x95,0xf6))
style_screen_settings_btn_s_back_main_main_default.set_border_width(0)
style_screen_settings_btn_s_back_main_main_default.set_border_opa(255)
style_screen_settings_btn_s_back_main_main_default.set_text_color(lv.color_make(0xff,0xff,0xff))
try:
    style_screen_settings_btn_s_back_main_main_default.set_text_font(lv.font_montserratMedium_16)
except AttributeError:
    try:
        style_screen_settings_btn_s_back_main_main_default.set_text_font(lv.font_montserrat_16)
    except AttributeError:
        style_screen_settings_btn_s_back_main_main_default.set_text_font(lv.font_montserrat_16)
style_screen_settings_btn_s_back_main_main_default.set_text_align(lv.TEXT_ALIGN.CENTER)

# add style for screen_settings_btn_s_back
screen_settings_btn_s_back.add_style(style_screen_settings_btn_s_back_main_main_default, lv.PART.MAIN|lv.STATE.DEFAULT)





def screen_settings_btn_s_back_clicked_1_event_cb(e,screen):
    src = e.get_target()
    code = e.get_code()
    lv.scr_load_anim(screen, lv.SCR_LOAD_ANIM.NONE, 100, 100, False)
screen_settings_btn_s_back.add_event_cb(lambda e: screen_settings_btn_s_back_clicked_1_event_cb(e,screen), lv.EVENT.CLICKED, None)




def screen_files_list_img_item0_clicked_1_event_cb(e,screen):
    src = e.get_target()
    code = e.get_code()
    lv.scr_load_anim(screen, lv.SCR_LOAD_ANIM.NONE, 100, 100, False)
screen_files_list_img_item0.add_event_cb(lambda e: screen_files_list_img_item0_clicked_1_event_cb(e,screen), lv.EVENT.CLICKED, None)





# content from custom.py

# Load the default screen
lv.scr_load(screen)

while SDL.check():
    time.sleep_ms(5)
