/*
 * Copyright 2023 NXP
 * SPDX-License-Identifier: MIT
 * The auto-generated can only be used on NXP devices
 */

#include "events_init.h"
#include <stdio.h>
#include "lvgl.h"
#include "custom.h"


void events_init(lv_ui *ui)
{
}

static void screen_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_SCREEN_LOAD_START:
	{
		screen_temp_cursor_create(guider_ui.screen_img_iray);
		lv_custom_img_iray_init(&guider_ui);
		lv_obj_add_flag(guider_ui.screen_img_iray, LV_OBJ_FLAG_CLICKABLE);
		screen_btnm_display(&guider_ui, false);
	}
		break;
	default:
		break;
	}
}

static void screen_img_iray_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_CLICKED:
	{
		screen_btnm_display_toggle(&guider_ui);
	}
		break;
	case LV_EVENT_DRAW_POST_END:
	{
		lv_custom_img_iray_updated();
	}
		break;
	default:
		break;
	}
}

static void screen_btnm_bot_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_PRESSED:
	{
		lv_obj_t *obj = lv_event_get_target(e);
		uint32_t id = lv_btnmatrix_get_selected_btn(obj);
		lv_obj_t * act_scr = lv_scr_act();
		lv_disp_t * d = lv_obj_get_disp(act_scr);
		if (d->prev_scr == NULL && (d->scr_to_load == NULL || d->scr_to_load == act_scr))
		{
		    if(strcmp("SETTING", lv_btnmatrix_get_btn_text(obj, id)) == 0)
		    {
		        if (guider_ui.screen_settings_del == true)
		            setup_scr_screen_settings(&guider_ui);
		        lv_scr_load_anim(guider_ui.screen_settings, LV_SCR_LOAD_ANIM_NONE, 100, 100, true);
		        guider_ui.screen_del = true;
		        guider_ui.screen = NULL;
		    }
		    else if(strcmp("FILE", lv_btnmatrix_get_btn_text(obj, id)) == 0)
		    {
		        if (guider_ui.screen_files_del == true)
		            setup_scr_screen_files(&guider_ui);
		        lv_scr_load_anim(guider_ui.screen_files, LV_SCR_LOAD_ANIM_NONE, 100, 100, true);
		        guider_ui.screen_del = true;
		        guider_ui.screen = NULL;
		    }
		    else if(strcmp("REC", lv_btnmatrix_get_btn_text(obj, id)) == 0)
		    {
		        screen_btnm_display(&guider_ui, false);
		        lv_custom_do_record();
		    }
		    else if(strcmp("CAP", lv_btnmatrix_get_btn_text(obj, id)) == 0)
		    {
		        screen_btnm_display(&guider_ui, false);
		        lv_custom_do_capture();
		    }
		}
		
	}
		break;
	default:
		break;
	}
}

void events_init_screen(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->screen, screen_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->screen_img_iray, screen_img_iray_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->screen_btnm_bot, screen_btnm_bot_event_handler, LV_EVENT_ALL, ui);
}

static void screen_files_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_SCREEN_LOADED:
	{
		lv_custom_files_list_update();
	}
		break;
	default:
		break;
	}
}

static void screen_files_img_files_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_CLICKED:
	{
		if(lv_obj_has_flag(guider_ui.screen_files_list_img, LV_OBJ_FLAG_HIDDEN))
		{
		    lv_obj_clear_flag(guider_ui.screen_files_list_img, LV_OBJ_FLAG_HIDDEN);
		}
		else
		{
		    lv_obj_add_flag(guider_ui.screen_files_list_img, LV_OBJ_FLAG_HIDDEN);
		}
	}
		break;
	default:
		break;
	}
}

static void screen_files_list_img_item0_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_CLICKED:
	{
		lv_obj_t * act_scr = lv_scr_act();
		lv_disp_t * d = lv_obj_get_disp(act_scr);
		if (d->prev_scr == NULL && (d->scr_to_load == NULL || d->scr_to_load == act_scr))
		{
			if (guider_ui.screen_del == true)
				setup_scr_screen(&guider_ui);
			lv_scr_load_anim(guider_ui.screen, LV_SCR_LOAD_ANIM_NONE, 100, 100, true);
			guider_ui.screen_files_del = true;
			guider_ui.screen_files = NULL;
		}
	}
		break;
	default:
		break;
	}
}

void events_init_screen_files(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->screen_files, screen_files_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->screen_files_img_files, screen_files_img_files_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->screen_files_list_img_item0, screen_files_list_img_item0_event_handler, LV_EVENT_ALL, ui);
}

static void screen_settings_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_SCREEN_LOADED:
	{
		int32_t value = screen_temp_cursor_get_num();
		lv_slider_set_value(guider_ui.screen_settings_slider_num, value, false);
		lv_label_set_text_fmt(guider_ui.screen_settings_label_value, "%d", value);
	}
		break;
	default:
		break;
	}
}

static void screen_settings_slider_num_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_VALUE_CHANGED:
	{
		int32_t value = lv_slider_get_value(guider_ui.screen_settings_slider_num);
		if(value < 1)
		{
		    value = 1;
		    lv_slider_set_value(guider_ui.screen_settings_slider_num, value, false);
		}
		lv_label_set_text_fmt(guider_ui.screen_settings_label_value, "%d", value);
		screen_temp_cursor_set_num(value);
	}
		break;
	default:
		break;
	}
}

static void screen_settings_btn_s_back_event_handler(lv_event_t *e)
{
	lv_event_code_t code = lv_event_get_code(e);
	switch (code)
	{
	case LV_EVENT_CLICKED:
	{
		lv_obj_t * act_scr = lv_scr_act();
		lv_disp_t * d = lv_obj_get_disp(act_scr);
		if (d->prev_scr == NULL && (d->scr_to_load == NULL || d->scr_to_load == act_scr))
		{
			if (guider_ui.screen_del == true)
				setup_scr_screen(&guider_ui);
			lv_scr_load_anim(guider_ui.screen, LV_SCR_LOAD_ANIM_NONE, 100, 100, false);
			guider_ui.screen_settings_del = false;
			guider_ui.screen_settings = NULL;
		}
	}
		break;
	default:
		break;
	}
}

void events_init_screen_settings(lv_ui *ui)
{
	lv_obj_add_event_cb(ui->screen_settings, screen_settings_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->screen_settings_slider_num, screen_settings_slider_num_event_handler, LV_EVENT_ALL, ui);
	lv_obj_add_event_cb(ui->screen_settings_btn_s_back, screen_settings_btn_s_back_event_handler, LV_EVENT_ALL, ui);
}
