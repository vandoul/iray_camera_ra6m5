/* generated vector source file - do not edit */
        #include "bsp_api.h"
        /* Do not build these data structures if no interrupts are currently allocated because IAR will have build errors. */
        #if VECTOR_DATA_IRQ_COUNT > 0
        BSP_DONT_REMOVE const fsp_vector_t g_vector_table[BSP_ICU_VECTOR_MAX_ENTRIES] BSP_PLACE_IN_SECTION(BSP_SECTION_APPLICATION_VECTORS) =
        {
                        [0] = sci_uart_rxi_isr, /* SCI4 RXI (Received data full) */
            [1] = sci_uart_txi_isr, /* SCI4 TXI (Transmit data empty) */
            [2] = sci_uart_tei_isr, /* SCI4 TEI (Transmit end) */
            [3] = sci_uart_eri_isr, /* SCI4 ERI (Receive error) */
            [4] = sci_i2c_txi_isr, /* SCI6 TXI (Transmit data empty) */
            [5] = sci_i2c_tei_isr, /* SCI6 TEI (Transmit end) */
            [6] = sci_spi_rxi_isr, /* SCI5 RXI (Received data full) */
            [7] = sci_spi_txi_isr, /* SCI5 TXI (Transmit data empty) */
            [8] = sci_spi_tei_isr, /* SCI5 TEI (Transmit end) */
            [9] = sci_spi_eri_isr, /* SCI5 ERI (Receive error) */
            [10] = sdhimmc_accs_isr, /* SDHIMMC0 ACCS (Card access) */
            [11] = sdhimmc_dma_req_isr, /* SDHIMMC0 DMA REQ (DMA transfer request) */
            [12] = r_icu_isr, /* ICU IRQ9 (External pin interrupt 9) */
            [13] = r_icu_isr, /* ICU IRQ10 (External pin interrupt 10) */
            [14] = sci_i2c_txi_isr, /* SCI2 TXI (Transmit data empty) */
            [15] = sci_i2c_tei_isr, /* SCI2 TEI (Transmit end) */
        };
        const bsp_interrupt_event_t g_interrupt_event_link_select[BSP_ICU_VECTOR_MAX_ENTRIES] =
        {
            [0] = BSP_PRV_IELS_ENUM(EVENT_SCI4_RXI), /* SCI4 RXI (Received data full) */
            [1] = BSP_PRV_IELS_ENUM(EVENT_SCI4_TXI), /* SCI4 TXI (Transmit data empty) */
            [2] = BSP_PRV_IELS_ENUM(EVENT_SCI4_TEI), /* SCI4 TEI (Transmit end) */
            [3] = BSP_PRV_IELS_ENUM(EVENT_SCI4_ERI), /* SCI4 ERI (Receive error) */
            [4] = BSP_PRV_IELS_ENUM(EVENT_SCI6_TXI), /* SCI6 TXI (Transmit data empty) */
            [5] = BSP_PRV_IELS_ENUM(EVENT_SCI6_TEI), /* SCI6 TEI (Transmit end) */
            [6] = BSP_PRV_IELS_ENUM(EVENT_SCI5_RXI), /* SCI5 RXI (Received data full) */
            [7] = BSP_PRV_IELS_ENUM(EVENT_SCI5_TXI), /* SCI5 TXI (Transmit data empty) */
            [8] = BSP_PRV_IELS_ENUM(EVENT_SCI5_TEI), /* SCI5 TEI (Transmit end) */
            [9] = BSP_PRV_IELS_ENUM(EVENT_SCI5_ERI), /* SCI5 ERI (Receive error) */
            [10] = BSP_PRV_IELS_ENUM(EVENT_SDHIMMC0_ACCS), /* SDHIMMC0 ACCS (Card access) */
            [11] = BSP_PRV_IELS_ENUM(EVENT_SDHIMMC0_DMA_REQ), /* SDHIMMC0 DMA REQ (DMA transfer request) */
            [12] = BSP_PRV_IELS_ENUM(EVENT_ICU_IRQ9), /* ICU IRQ9 (External pin interrupt 9) */
            [13] = BSP_PRV_IELS_ENUM(EVENT_ICU_IRQ10), /* ICU IRQ10 (External pin interrupt 10) */
            [14] = BSP_PRV_IELS_ENUM(EVENT_SCI2_TXI), /* SCI2 TXI (Transmit data empty) */
            [15] = BSP_PRV_IELS_ENUM(EVENT_SCI2_TEI), /* SCI2 TEI (Transmit end) */
        };
        #endif